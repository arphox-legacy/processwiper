﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Windows.Threading;

namespace ProcessWiper
{
    static class DataProvider
    {
        private static DispatcherTimer timer = new DispatcherTimer();

        private static string[] processes;
        public static string[] Processes
        {
            get
            {
                return processes;
            }
        }

        private static string[] titles;
        public static string[] Titles
        {
            get
            {
                return titles;
            }
        }

        public static void Init()
        {
            SetStartingData();

            timer.Interval = Config.INTERVAL_NetDataCheck;
            timer.Tick += TryToGetDataFromNet;
            timer.Start();
        }

        private static void SetStartingData()
        {
            TryToGetDataFromNet(null, null); //Try getting it from the net

            if (titles == null || processes == null) //If can't get data from net
            {
                TryToGetDataFromLOCAL();

                if (titles == null || processes == null)   //If can't get data from local
                {
                    titles = BackupData.FilteredNames;
                    processes = BackupData.FilteredProcesses;
                }
            }
        }
        private static void TryToGetDataFromNet(object sender, EventArgs e)
        {
            try
            {
                using (WebClient wc = new WebClient())
                {
                    titles = wc.DownloadString(Config.ADDRESS_Titles)
                        .Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

                    processes = wc.DownloadString(Config.ADDRRESS_Processes)
                        .Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

                    Debug.WriteLine(DateTime.Now.ToString() + ": Getting data from net is successful.");
                }
            }
            catch (Exception) { }
        }
        private static void TryToGetDataFromLOCAL()
        {
            try
            {
                if (!File.Exists(Config.LOCALNAME_Titles) ||
                    !File.Exists(Config.LOCALNAME_Processes))
                    return;

                titles = File.ReadAllLines(Config.LOCALNAME_Titles);
                //File.Delete(Config.LOCALNAME_Titles);

                processes = File.ReadAllLines(Config.LOCALNAME_Processes);
                //File.Delete(Config.LOCALNAME_Processes);
            }
            catch (Exception) { }
        }
    }
    class BackupData
    {
        public static string[] FilteredProcesses =
        {
            "LoLPatcher",
            "LolClient",
            "Microsoft Edge",
            "MicrosoftEdge"
        };
        public static string[] FilteredNames =
        {
            "VPN",
            "Chrome Internetes áruház", "Firefox-kiegészítők",

            "Facebook", "Instagram", "Google+", "Google Plus", //"Twitter", 
            "Honfoglaló", "Goodgame", "Farmerama", "TRAVIAN",
            "Kongregate", "Games-4-Free", "Miniclip", "Free Online Game", "Addicting Games", "Big Fish Games", "FOG.COM", "Pokemon.com",
            "Twitch", "9GAG", "napiszar.com", "Napiszar",
            "io Games", "iogames.space", "slither.io", "Agar.io", "diep.io", "Limax.io", "Wings!", "Supersnake.io", "Slay.one", "Tank Wars!", "Wilds.io", "War In Space", "Treasure Arena", "Vanar.io",  "Acyd", "Piranh.io", "GeoArena", "Spheroids", "tankario", "Lazerdrive", "Biome 3D", "Timaleon", "Game of Bombs", "Massive Match", "TagPro", "Curve Fever",

            "World of Warcraft",
            "Minecraft",
            "Hearthstone",
            "Battle.net",
            "TeamViewer"
        };
    }

}
