﻿using System;

namespace ProcessWiper
{
    static class Config
    {
        public const string ADDRESS_LogDirectory = @"\\praxis\na$\log\";
        public const string ADDRESS_Titles = @"\\praxis\na$\mwtitles.txt";
        public const string ADDRRESS_Processes = @"\\praxis\na$\processes.txt";

        public const string LOCALNAME_Titles = @"mwtitles.txt";
        public const string LOCALNAME_Processes = @"processes.txt";

        public static TimeSpan INTERVAL_NetDataCheck = new TimeSpan(0, 1, 0);
        public static TimeSpan INTERVAL_ProcessCheck = new TimeSpan(0, 0, 3);
        public static TimeSpan INTERVAL_Log = new TimeSpan(0, 20, 0);

        public static byte LOGMODE = 2; //0 = No log, 1 = local log, 2 = network log
    }
}