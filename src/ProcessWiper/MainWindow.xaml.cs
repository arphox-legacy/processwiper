﻿using System.Windows;

namespace ProcessWiper
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ViewModel vm = new ViewModel();

        public MainWindow()
        {
            InitializeComponent();
            this.ShowInTaskbar = false;
            this.Hide();
        }
    }
}
